# AstroNeat

![screenshot](screenshot.png "Generated AI playing AstroNeat")

Astroneat is a remake of the classic _Asteroids_ game using the [piston][piston] game
engine. Included is a system for generating AI players using the NEAT algorithm
via [rsneat][rsneat]


Hyperspace font: [pixelsagas][pixelsagas]

[pixelsagas]: http://www.pixelsagas.com/?download=hyperspace
[piston]: https://github.com/PistonDevelopers/piston
[rsneat]: https://gitlab.com/robert-hrusecky/rsneat

## Human controls

 * Left: Turn left.
 * Right: Turn right.
 * Forward: Activate thrusters.
 * Space: Shoot.
 * Hold Space: Triple shot.
