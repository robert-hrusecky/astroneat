extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston;
extern crate rand;
extern crate texture;

mod game;

use game::screen::{Back, MenuScreen, Screen, Switch};
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, GlyphCache, OpenGL};
use piston::event_loop::*;
use piston::input::*;
use piston::window::WindowSettings;
// use graphics::glyph_cache::rusttype::GlyphCache;
use texture::TextureSettings;

pub struct App {
    window: Window, // Glutin window
    glyph_cache: GlyphCache<'static>,
    gl: GlGraphics,              // OpenGL drawing backend.
    stack: Vec<Box<dyn Screen>>, // stack for screen traversal
}

impl App {
    fn new(opengl: OpenGL) -> Self {
        let mut result = Self {
            window: WindowSettings::new("AstroNeat", [800, 600])
                .opengl(opengl)
                .exit_on_esc(true)
                .build()
                .unwrap(),
            glyph_cache: GlyphCache::from_bytes(
                include_bytes!("../Hyperspace.otf"),
                (),
                TextureSettings::new(),
            )
            .unwrap(),
            gl: GlGraphics::new(opengl),
            stack: Vec::new(),
        };
        result.stack.push(Box::new(MenuScreen::new()));

        result
    }

    fn run(&mut self) {
        // run the game loop
        let mut events = Events::new(EventSettings::new()).ups(60);
        let mut cursor = [0.0, 0.0];
        while let Some(e) = events.next(&mut self.window) {
            if let Some(u) = e.update_args() {
                self.update(&u);
                if self.stack.len() == 0 {
                    break;
                }
            }
            e.mouse_cursor(|x, y| cursor = [x, y]);

            let screen = self.stack.last_mut().unwrap();
            if let Some(button) = e.press_args() {
                match button {
                    Button::Keyboard(key) => screen.key_press(key),
                    Button::Mouse(button) => screen.mouse_press(button, cursor),
                    _ => (),
                }
            }
            if let Some(button) = e.release_args() {
                match button {
                    Button::Keyboard(key) => screen.key_release(key),
                    Button::Mouse(button) => screen.mouse_release(button, cursor),
                    _ => (),
                }
            }

            if let Some(r) = e.render_args() {
                self.render(&r);
            }
        }
    }

    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
        let screen = &self.stack.last().unwrap();
        let glyph_cache = &mut self.glyph_cache;

        self.gl.draw(args.viewport(), |c, gl| {
            clear(BLACK, gl);
            screen.draw(&c, gl, glyph_cache);
        });
    }

    fn update(&mut self, _args: &UpdateArgs) {
        let switch = self.stack.last_mut().unwrap().update();
        if let Switch(screen) = switch {
            self.stack.push(screen);
        } else if let Back = switch {
            self.stack.pop();
        }
    }
}

fn main() {
    // Change this to OpenGL::V2_1 if not working.
    let opengl = OpenGL::V3_2;

    // Create a new game and run it.
    let mut app = App::new(opengl);
    app.run();
}
