use graphics::{context::Context, math, math::Vec2d};
use opengl_graphics::GlGraphics;
use super::Entity;
use super::Wrappable;

pub struct Bullet {
    pub x: Vec2d<f64>,
    pub v: Vec2d<f64>,
    pub timer: u32,
}

impl Entity for Bullet {
    fn draw(&self, c: &Context, g: &mut GlGraphics) {
        use graphics::*;
        const BLUE: [f32; 4] = [0.0, 1.0, 0.0, 1.0];
        let transform = c.transform.trans(self.x[0], self.x[1]);

        ellipse(BLUE, [-2.0, -2.0, 2.0, 2.0], transform, g);
    }

    fn update(&mut self) {
        if self.timer > 0 {
            self.x = math::add(self.x, self.v);
            self.timer -= 1;
        }

        self.x.wrap();
    }

    fn x(&mut self) -> &mut Vec2d<f64> {
        &mut self.x
    }
}
