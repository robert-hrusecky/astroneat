use super::Entity;
use super::Wrappable;
use graphics::{context::Context, math, math::Vec2d};
use opengl_graphics::GlGraphics;
use rand;
use std::f64::consts::PI;

pub struct Asteroid {
    pub x: Vec2d<f64>,
    pub v: Vec2d<f64>,
    pub radius: f64,
}

impl Asteroid {
    pub fn new() -> Self {
        let angle = 2.0 * PI * rand::random::<f64>();
        let len = rand::random::<f64>() + 1.0;
        Self {
            x: [800.0 * rand::random::<f64>(), 600.0 * rand::random::<f64>()],
            v: math::mul_scalar([angle.cos(), angle.sin()], len),
            radius: 50.0,
        }
    }

    fn childeren(self) -> Vec<Self> {
        let radius = self.radius / 2.0;
        let angle = 2.0 * PI * rand::random::<f64>();
        let len1 = 100.0 * rand::random::<f64>() / self.radius;
        let len2 = 100.0 * rand::random::<f64>() / self.radius;
        let v1 = math::mul_scalar([angle.cos(), angle.sin()], len1);
        let v2 = math::sub(math::mul_scalar(self.v, 4.0), v1);
        let v2 = math::mul_scalar(v2, len2 / math::square_len(v2).sqrt());
        vec![
            Self {
                v: v1,
                radius,
                ..self
            },
            Self {
                v: v2,
                radius,
                ..self
            },
        ]
    }

    pub fn pieces(self) -> Option<Vec<Self>> {
        if self.radius > 50.0 / 4.0 {
            Some(self.childeren())
        } else {
            None
        }
    }
}

impl Entity for Asteroid {
    fn draw(&self, c: &Context, g: &mut GlGraphics) {
        use graphics::*;
        const WHITE: [f32; 4] = [1.0, 1.0, 1.0, 1.0];
        let transform = c.transform.trans(self.x[0], self.x[1]);

        circle_arc(
            WHITE,
            0.5,
            0.01,
            2.0 * PI,
            [
                -self.radius,
                -self.radius,
                2.0 * self.radius,
                2.0 * self.radius,
            ],
            transform,
            g,
        );
    }

    fn update(&mut self) {
        self.x = math::add(self.x, self.v);
        self.x.wrap();
    }

    fn x(&mut self) -> &mut Vec2d<f64> {
        &mut self.x
    }
}
