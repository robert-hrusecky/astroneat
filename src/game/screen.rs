use graphics::context::Context;
use graphics::math::Vec2d;
use opengl_graphics::{GlGraphics, GlyphCache};
use piston::input::{Key, MouseButton};
use rsneat::genome::Genome;
use rsneat::network::Network;
use std::fs::File;

use super::controls::{Controls, NetworkControls, UserControls};
use super::neat;
use super::Game;

use std::sync::{Arc, Mutex};
use std::thread;

pub use ScreenSwitch::{Back, Continue, Switch};

pub enum ScreenSwitch<T> {
	Switch(T),
	Continue,
	Back,
}

pub trait Screen {
	fn draw(&self, c: &Context, g: &mut GlGraphics, glyph_cache: &mut GlyphCache);
	fn update(&mut self) -> ScreenSwitch<Box<dyn Screen>>;
	fn key_press(&mut self, key: Key);
	fn key_release(&mut self, key: Key);
	fn mouse_press(&mut self, button: MouseButton, cursor: Vec2d<f64>);
	fn mouse_release(&mut self, button: MouseButton, cursor: Vec2d<f64>);
}

pub struct GameScreen {
	game: Game<UserControls>,
}

impl GameScreen {
	pub fn new() -> Self {
		Self {
			game: Game::new(UserControls::new()),
		}
	}
}

impl Screen for GameScreen {
	fn draw(&self, c: &Context, g: &mut GlGraphics, glyph_cache: &mut GlyphCache) {
		self.game.draw(c, g, glyph_cache);
	}

	fn update(&mut self) -> ScreenSwitch<Box<dyn Screen>> {
		if self.game.update() {
			Continue
		} else {
			Back
		}
	}

	fn key_press(&mut self, key: Key) {
		self.game.controls().key_press(key);
	}

	fn key_release(&mut self, key: Key) {
		self.game.controls().key_release(key);
	}

	fn mouse_press(&mut self, _button: MouseButton, _cursor: Vec2d<f64>) {}
	fn mouse_release(&mut self, _button: MouseButton, _cursor: Vec2d<f64>) {}
}

pub struct DemoScreen {
	game: Game<NetworkControls>,
}

impl DemoScreen {
	pub fn new(net: Network) -> Self {
		Self {
			game: Game::new(NetworkControls::new(net)),
		}
	}
}

impl Screen for DemoScreen {
	fn draw(&self, c: &Context, g: &mut GlGraphics, glyph_cache: &mut GlyphCache) {
		self.game.draw(c, g, glyph_cache);
	}

	fn update(&mut self) -> ScreenSwitch<Box<dyn Screen>> {
		if self.game.update() {
			Continue
		} else {
			Back
		}
	}

	fn key_press(&mut self, key: Key) {
		self.game.controls().key_press(key);
	}

	fn key_release(&mut self, key: Key) {
		self.game.controls().key_release(key);
	}

	fn mouse_press(&mut self, _button: MouseButton, _cursor: Vec2d<f64>) {}
	fn mouse_release(&mut self, _button: MouseButton, _cursor: Vec2d<f64>) {}
}

pub struct NeatScreen {
	champ_mutex: Arc<Mutex<Option<Network>>>,
	done: bool,
	angle: f64,
}

impl NeatScreen {
	pub fn new() -> Self {
		let result = Self {
			champ_mutex: Arc::new(Mutex::new(None)),
			done: false,
			angle: 0.0,
		};
		let champ_mutex = Arc::clone(&result.champ_mutex);
		thread::spawn(move || neat::neat(champ_mutex));
		result
	}
}

impl Screen for NeatScreen {
	fn draw(&self, c: &Context, g: &mut GlGraphics, glyph_cache: &mut GlyphCache) {
		use graphics::*;
		const WHITE: [f32; 4] = [1.0, 1.0, 1.0, 1.0];
		let transform = c
			.transform
			.trans(400.0, 300.0)
			.rot_rad(self.angle)
			.trans(-10.0, 0.0);
		text(WHITE, 50, "Neating...", glyph_cache, transform, g).unwrap();
	}

	fn update(&mut self) -> ScreenSwitch<Box<dyn Screen>> {
		let mut network = None;
		let mut new_network = self.champ_mutex.lock().unwrap();
		std::mem::swap(&mut network, &mut *new_network);
		if let Some(network) = network {
			self.done = true;
			return Switch(Box::new(DemoScreen::new(network)));
		} else {
			self.angle += 0.01;
		}
		if self.done {
			Back
		} else {
			Continue
		}
	}

	fn key_press(&mut self, _key: Key) {}
	fn key_release(&mut self, _key: Key) {}
	fn mouse_press(&mut self, _button: MouseButton, _cursor: Vec2d<f64>) {}
	fn mouse_release(&mut self, _button: MouseButton, _cursor: Vec2d<f64>) {}
}

pub struct MenuScreen {
	next: Option<Box<dyn Screen>>,
}

impl MenuScreen {
	pub fn new() -> Self {
		Self { next: None }
	}
}

impl Screen for MenuScreen {
	fn draw(&self, c: &Context, g: &mut GlGraphics, glyph_cache: &mut GlyphCache) {
		use graphics::*;
		const WHITE: [f32; 4] = [1.0, 1.0, 1.0, 1.0];
		let play_transform = c.transform.trans(300.0, 300.0);
		text(WHITE, 30, "Play", glyph_cache, play_transform, g).unwrap();

		line(WHITE, 1.0, [400.0, 0.0, 400.0, 600.0], c.transform, g);

		let neat_transform = c.transform.trans(425.0, 300.0);
		text(WHITE, 30, "Neat", glyph_cache, neat_transform, g).unwrap();

		line(WHITE, 1.0, [600.0, 0.0, 600.0, 600.0], c.transform, g);

		let neat_transform = c.transform.trans(625.0, 300.0);
		text(WHITE, 30, "Load", glyph_cache, neat_transform, g).unwrap();
	}

	fn update(&mut self) -> ScreenSwitch<Box<dyn Screen>> {
		let mut option_screen = None;
		std::mem::swap(&mut self.next, &mut option_screen);
		if let Some(screen) = option_screen {
			Switch(screen)
		} else {
			Continue
		}
	}

	fn key_press(&mut self, _key: Key) {}
	fn key_release(&mut self, _key: Key) {}
	fn mouse_press(&mut self, _button: MouseButton, _cursor: Vec2d<f64>) {}

	fn mouse_release(&mut self, button: MouseButton, cursor: Vec2d<f64>) {
		if button == MouseButton::Left {
			self.next = if cursor[0] < 400.0 {
				Some(Box::new(GameScreen::new()))
			} else if cursor[0] < 600.0 {
				Some(Box::new(NeatScreen::new()))
			} else if let Ok(mut file) = File::open("demo.neat") {
				Some(Box::new(DemoScreen::new(Network::from_genome(
					&Genome::read_from(&mut file).unwrap(),
				))))
			} else {
				None
			};
		}
	}
}
