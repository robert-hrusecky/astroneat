use graphics::{context::Context, math, math::Vec2d};
use opengl_graphics::{GlGraphics, GlyphCache};
use piston::input::Key;
use rsneat::network::Network;

use super::Asteroid;
use super::Ship;
use super::Wrappable;

use std::f64::consts::PI;

pub const DIRECTIONS: usize = 16;
pub const INPUTS: usize = 2 * DIRECTIONS + 3;

pub trait Controls {
	fn draw(
		&self,
		c: &Context,
		g: &mut GlGraphics,
		glyph_cache: &mut GlyphCache,
		asteroids: &Vec<Asteroid>,
		ship: &Ship,
	);
	fn key_press(&mut self, key: Key);
	fn key_release(&mut self, key: Key);
	fn update(&mut self, asteroids: &Vec<Asteroid>, ship: &Ship);
	fn is_left_pressed(&self) -> bool;
	fn is_right_pressed(&self) -> bool;
	fn is_thrust_pressed(&self) -> bool;
	fn is_fire_pressed(&self) -> bool;
}

pub struct UserControls {
	key_up: bool,
	key_left: bool,
	key_right: bool,
	key_space: bool,
}

impl UserControls {
	pub fn new() -> Self {
		Self {
			key_up: false,
			key_left: false,
			key_right: false,
			key_space: false,
		}
	}
}

impl Controls for UserControls {
	fn draw(
		&self,
		_c: &Context,
		_g: &mut GlGraphics,
		_glyph_cache: &mut GlyphCache,
		_asteroids: &Vec<Asteroid>,
		_ship: &Ship,
	) {
	} // unused

	fn key_press(&mut self, key: Key) {
		if key == Key::Up {
			self.key_up = true;
		}

		if key == Key::Left {
			self.key_left = true;
		}
		if key == Key::Right {
			self.key_right = true;
		}
		if key == Key::Space {
			self.key_space = true;
		}
	}

	fn key_release(&mut self, key: Key) {
		if key == Key::Up {
			self.key_up = false;
		}

		if key == Key::Left {
			self.key_left = false;
		}
		if key == Key::Right {
			self.key_right = false;
		}
		if key == Key::Space {
			self.key_space = false;
		}
	}

	fn update(&mut self, _asteroids: &Vec<Asteroid>, _ship: &Ship) {} // unused

	fn is_left_pressed(&self) -> bool {
		self.key_left
	}

	fn is_right_pressed(&self) -> bool {
		self.key_right
	}

	fn is_thrust_pressed(&self) -> bool {
		self.key_up
	}

	fn is_fire_pressed(&self) -> bool {
		self.key_space
	}
}

pub struct NetworkControls {
	net: Network,
	output: Vec<bool>,
	show: bool,
}

impl NetworkControls {
	pub fn new(net: Network) -> Self {
		Self {
			net,
			output: vec![false; 4],
			show: false,
		}
	}

	fn scan_in_direction(
		mut p: Vec2d<f64>,
		v: Vec2d<f64>,
		d: Vec2d<f64>,
		asteroids: &Vec<Asteroid>,
	) -> [f64; 2] {
		let mut len = 0.0;
		let d_len = math::square_len(d).sqrt();
		while len < 400.0 {
			if let Some(a) = asteroids
				.iter()
				.find(|a| math::square_len(math::sub(a.x, p)) < a.radius * a.radius)
			{
				return [2.0 / (len + 2.0), math::dot(d, math::sub(a.v, v))];
			}

			len += d_len;
			p = math::add(p, d);
			p.wrap();
		}
		[0.0, 0.0]
	}

	fn draw_in_direction(
		mut p: Vec2d<f64>,
		d: Vec2d<f64>,
		asteroids: &Vec<Asteroid>,
		c: &Context,
		g: &mut GlGraphics,
	) {
		use graphics::*;
		let mut len = 0.0;
		let d_len = math::square_len(d).sqrt();
		while len < 400.0 {
			len += d_len;
			p = math::add(p, d);
			p.wrap();

			if let Some(_a) = asteroids
				.iter()
				.find(|a| math::square_len(math::sub(a.x, p)) < a.radius * a.radius)
			{
				use graphics::*;
				const BLUE: [f32; 4] = [0.0, 1.0, 0.0, 1.0];
				let transform = c.transform.trans(p[0], p[1]);
				ellipse(BLUE, [-4.0, -4.0, 8.0, 8.0], transform, g);
				break;
			}
		}
	}
}

impl Controls for NetworkControls {
	fn draw(
		&self,
		c: &Context,
		g: &mut GlGraphics,
		_glyph_cache: &mut GlyphCache,
		asteroids: &Vec<Asteroid>,
		ship: &Ship,
	) {
		if self.show {
			for i in 0..DIRECTIONS {
				let angle = ship.t + i as f64 * 2.0 * PI / DIRECTIONS as f64;
				Self::draw_in_direction(
					ship.x,
					[4.0 * angle.cos(), 4.0 * angle.sin()],
					asteroids,
					c,
					g,
				);
			}
		}
	}

	fn key_press(&mut self, _key: Key) {} // unused
	fn key_release(&mut self, key: Key) {
		if key == Key::Space {
			self.show = !self.show;
		}
	}

	fn update(&mut self, asteroids: &Vec<Asteroid>, ship: &Ship) {
		let mut input = Vec::new();
		input.reserve_exact(INPUTS);
		for i in 0..DIRECTIONS {
			let angle = ship.t + i as f64 * 2.0 * PI / DIRECTIONS as f64;
			let result = Self::scan_in_direction(
				ship.x,
				ship.v,
				[4.0 * angle.cos(), 4.0 * angle.sin()],
				asteroids,
			);
			input.push(result[0]);
			input.push(result[1]);
		}
		input.push(ship.v[1].atan2(ship.v[0]));
		input.push(math::square_len(ship.v).sqrt());
		input.push(1.0);
		self.output = self
			.net
			.activate(input.into_iter())
			.map(|a| a >= 0.5)
			.collect();
	}

	fn is_left_pressed(&self) -> bool {
		self.output[0]
	}

	fn is_right_pressed(&self) -> bool {
		self.output[1]
	}

	fn is_thrust_pressed(&self) -> bool {
		self.output[2]
	}

	fn is_fire_pressed(&self) -> bool {
		self.output[3]
	}
}
