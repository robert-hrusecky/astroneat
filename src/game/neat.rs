use super::controls::{NetworkControls, INPUTS};
use super::Game;

use rsneat::{network::Network, population::Population, Neat};
use std::fs::File;
use std::sync::{Arc, Mutex};

pub fn neat(champ_mutex: Arc<Mutex<Option<Network>>>) {
	let mut neat = Neat::new();
	// let mut founder = Genome::new(INPUTS, 4);
	// for input in 0..INPUTS {
	// 	for output in INPUTS..INPUTS + 4 {
	// 		founder.add_connection(input, output, 0.0, &mut neat);
	// 	}
	// }
	// let mut pop = Population::clone_from(founder, &neat);
	let mut pop = Population::new(INPUTS, 4, &mut neat);
	let mut max_fitness = 0.0;
	while max_fitness < 30.0 && pop.gen() < 1000 {
		max_fitness = pop.quick_generation(
			|n| {
				let mut test_game = Game::new(NetworkControls::new(n));
				let frame_limit = 60 * 60 * 4;
				let mut frames = 0;
				while frames < frame_limit && test_game.update() {
					frames += 1;
				}
				// if frames == frame_limit {
				// 	println!("swole dude detected");
				// }
				let fit = frames as f64 / frame_limit as f64 * test_game.score() as f64;
				fit
			},
			50,
			&mut neat,
		);
		println!(
			"Gen {}: {}, Species: {}",
			pop.gen(),
			max_fitness,
			pop.species_count()
		);
	}
	let mut option_champ = champ_mutex.lock().unwrap();

	for (i, champ) in pop.champs().iter().enumerate() {
		champ
			.write(&mut File::create(format!("species{}.neat", i)).unwrap())
			.unwrap();
	}

	pop.champ()
		.write(&mut File::create("demo.neat").unwrap())
		.unwrap();

	*option_champ = Some(Network::from_genome(pop.champ()));
}
