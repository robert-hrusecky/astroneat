use super::asteroid::Asteroid;
use super::bullet::Bullet;
use super::{ship, ship::Ship};

use graphics::{math, math::Vec2d};

pub fn is_colliding_ab(asteroid: &Asteroid, bullet: &Bullet) -> bool {
	let delta = math::add(asteroid.x, math::mul_scalar(bullet.x, -1.0));
	math::square_len(delta) < asteroid.radius * asteroid.radius
}

fn sproj(v: Vec2d<f64>, u: Vec2d<f64>) -> f64 {
	math::dot(u, v) / math::dot(v, v).sqrt()
}

fn is_colliding_axis(
	p1: Vec2d<f64>,
	p2: Vec2d<f64>,
	p3: Vec2d<f64>,
	center: Vec2d<f64>,
	radius: f64,
) -> bool {
	let axis = math::perp(math::sub(p3, p2));

	let sp1 = sproj(axis, p1);
	let sp2 = sproj(axis, p2);
	let sp = (sp1 + sp2) / 2.0;
	let pr = (sp1 - sp2).abs() / 2.0;
	let sc = sproj(axis, center);
	(sp - sc).abs() <= pr + radius
}

pub fn is_colliding_as(asteroid: &Asteroid, ship: &Ship) -> bool {
	let rot = math::rotate_radians(ship.t);
	let p1 = math::add(ship.x, math::transform_vec(rot, ship::P1));
	let p2 = math::add(ship.x, math::transform_vec(rot, ship::P2));
	let p3 = math::add(ship.x, math::transform_vec(rot, ship::P3));

	is_colliding_axis(p1, p2, p3, asteroid.x, asteroid.radius)
		&& is_colliding_axis(p2, p3, p1, asteroid.x, asteroid.radius)
		&& is_colliding_axis(p3, p1, p2, asteroid.x, asteroid.radius)
}
