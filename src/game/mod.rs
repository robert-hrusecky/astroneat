mod asteroid;
mod bullet;
mod collision;
pub mod controls;
mod neat;
pub mod screen;
mod ship;

pub use self::asteroid::Asteroid;
pub use self::bullet::Bullet;
pub use self::ship::Ship;

use self::controls::Controls;
use graphics::{context::Context, math, math::Vec2d};
use opengl_graphics::{GlGraphics, GlyphCache};

trait Entity {
    fn draw(&self, c: &Context, g: &mut GlGraphics);
    fn update(&mut self);
    fn x(&mut self) -> &mut Vec2d<f64>;
}

trait Wrappable {
    fn wrap(&mut self);
}

impl Wrappable for Vec2d<f64> {
    fn wrap(&mut self) {
        if self[0] > 800.0 {
            self[0] = 0.0;
        } else if self[0] < 0.0 {
            self[0] = 800.0;
        }

        if self[1] > 600.0 {
            self[1] = 0.0;
        } else if self[1] < 0.0 {
            self[1] = 600.0;
        }
    }
}

pub struct Game<T> {
    asteroids: Vec<Asteroid>,
    bullets: Vec<Bullet>,
    bullet_timer: i32,
    ship: Ship,
    score: u64,
    controls: T,
}

impl<T: Controls> Game<T> {
    pub fn new(controls: T) -> Self {
        let mut result = Self {
            asteroids: Vec::new(),
            bullets: Vec::new(),
            bullet_timer: 0,
            ship: Ship::new(),
            score: 0,
            controls,
        };
        result.reset();
        result
    }

    pub fn controls(&mut self) -> &mut T {
        &mut self.controls
    }

    pub fn score(&self) -> u64 {
        self.score
    }

    pub fn draw(&self, c: &Context, g: &mut GlGraphics, glyph_cache: &mut GlyphCache) {
        use graphics::*;
        const WHITE: [f32; 4] = [1.0, 1.0, 1.0, 1.0];
        for a in &self.asteroids {
            a.draw(c, g);
        }
        for b in &self.bullets {
            b.draw(c, g);
        }
        self.ship.draw(c, g);
        let transform = c.transform.trans(20.0, 20.0);
        text(
            WHITE,
            20,
            &format!("Score: {}", self.score()),
            glyph_cache,
            transform,
            g,
        )
        .unwrap();

        self.controls.draw(c, g, glyph_cache, &self.asteroids, &self.ship);
    }

    pub fn update(&mut self) -> bool {
        // update values based on input
        self.ship.a = if self.controls.is_thrust_pressed() {
            0.2
        } else {
            0.0
        };
        self.ship.vt = 0.0;
        if self.controls.is_left_pressed() {
            self.ship.vt -= 0.1;
        }
        if self.controls.is_right_pressed() {
            self.ship.vt += 0.1;
        }

        // update the ship's bullet timer
        if self.bullet_timer == 0 && self.controls.is_fire_pressed() {
            self.bullet_timer = 60;
            self.spawn_bullet();
        }
        if self.bullet_timer > 0 {
            if !self.controls.is_fire_pressed() {
                self.bullet_timer = -6;
            } else {
                self.bullet_timer -= 1;
                if self.bullet_timer < 15 {
                    if self.bullet_timer % 5 == 0 {
                        self.spawn_bullet();
                    }
                }
                if self.bullet_timer == 0 {
                    self.bullet_timer = -1;
                }
            }
        }
        if self.bullet_timer < -1 {
            self.bullet_timer += 1;
        } else if self.bullet_timer < 0 && !self.controls.is_fire_pressed() {
            self.bullet_timer = 0;
        }

        // update entities
        for a in &mut self.asteroids {
            a.update();
        }
        for b in &mut self.bullets {
            b.update();
        }

        // check for collisions
        let len = self.asteroids.len();
        for i in 0..len {
            let idx = len - i - 1;
            if let Some(j) = self
                .bullets
                .iter()
                .position(|b| collision::is_colliding_ab(&self.asteroids[idx], b))
            {
                self.bullets.swap_remove(j);
                let old_asteroid = self.asteroids.swap_remove(idx);
                self.spawn(old_asteroid);
                self.score += 1;
            } else if collision::is_colliding_as(&self.asteroids[idx], &self.ship) {
                return false;
            }
        }

        let len = self.bullets.len();
        for i in 0..len {
            let idx = len - i - 1;
            if self.bullets[idx].timer == 0 {
                self.bullets.swap_remove(idx);
            }
        }
        self.ship.update();

        if self.asteroids.len() == 0 {
            self.reset();
        }

        self.controls.update(&self.asteroids, &self.ship);
        true
    }

    fn spawn(&mut self, old_asteroid: Asteroid) {
        if let Some(mut news) = old_asteroid.pieces() {
            self.asteroids.append(&mut news);
        }
    }

    fn spawn_bullet(&mut self) {
        let ship_dir = [self.ship.t.cos(), self.ship.t.sin()];
        self.bullets.push(Bullet {
            x: math::add(self.ship.x, math::mul_scalar(ship_dir, 16.0)),
            v: math::add(self.ship.v, math::mul_scalar(ship_dir, 10.0)),
            timer: 40,
        });
    }

    fn reset(&mut self) {
        self.asteroids.clear();
        self.bullets.clear();
        for _ in 0..7 {
            loop {
                let attempt = Asteroid::new();
                let min_dist = 4.0 * attempt.radius;
                let dist = math::sub(attempt.x, self.ship.x);
                if math::square_len(dist) > min_dist * min_dist {
                    self.asteroids.push(attempt);
                    break;
                }
            }
        }
    }
}
