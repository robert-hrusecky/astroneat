use super::Entity;
use super::Wrappable;
use graphics::{context::Context, math, math::Vec2d};
use opengl_graphics::GlGraphics;
use std::f64::consts::PI;

pub const P1: Vec2d<f64> = [16.0, 0.0];
pub const P2: Vec2d<f64> = [-4.0, -8.0];
pub const P3: Vec2d<f64> = [-4.0, 8.0];

pub struct Ship {
    pub x: Vec2d<f64>,
    pub v: Vec2d<f64>,
    pub a: f64,
    pub t: f64,
    pub vt: f64,
}

impl Ship {
    pub fn new() -> Self {
        Self {
            x: [400.0, 300.0],
            v: [0.0, 0.0],
            a: 0.0,
            t: -PI / 2.0,
            vt: 0.0,
        }
    }
}

impl Entity for Ship {
    fn draw(&self, c: &Context, g: &mut GlGraphics) {
        use graphics::*;
        const WHITE: [f32; 4] = [1.0, 1.0, 1.0, 1.0];
        const RED: [f32; 4] = [1.0, 0.0, 0.0, 1.0];
        let transform = c.transform.trans(self.x[0], self.x[1]).rot_rad(self.t);

        line(WHITE, 0.5, [P1[0], P1[1], P2[0], P2[1]], transform, g);
        line(WHITE, 0.5, [P2[0], P2[1], P3[0], P3[1]], transform, g);
        line(WHITE, 0.5, [P3[0], P3[1], P1[0], P1[1]], transform, g);

        if self.a > 0.0 {
            let p1 = [-8.0, 0.0];
            let p2 = [-4.0, -4.0];
            let p3 = [-4.0, 4.0];
            line(RED, 0.5, [p1[0], p1[1], p2[0], p2[1]], transform, g);
            // line(RED, 0.5, [p2[0], p2[1], p3[0], p3[1]], transform, g);
            line(RED, 0.5, [p3[0], p3[1], p1[0], p1[1]], transform, g);
        }
    }

    fn update(&mut self) {
        self.t += self.vt;
        let a = math::mul_scalar([self.t.cos(), self.t.sin()], self.a);

        let mut new_v = math::add(self.v, a);
        if math::square_len(new_v) > 0.01 * 0.01 {
            new_v = math::add(
                new_v,
                math::mul_scalar(new_v, -0.01 / math::square_len(new_v).sqrt()),
            );
        } else {
            new_v = [0.0, 0.0];
        }
        let speed = math::square_len(new_v).sqrt();
        if speed > 10.0 {
            new_v = math::mul_scalar(new_v, 10.0 / speed);
        }

        self.v = new_v;

        self.x = math::add(self.x, self.v);

        self.x.wrap();
    }

    fn x(&mut self) -> &mut Vec2d<f64> {
        &mut self.x
    }
}
